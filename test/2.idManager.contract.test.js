const ethers = require('ethers')
const expect = require('chai').expect
const { BN, expectEvent } = require('openzeppelin-test-helpers')
const expectThrow = require('./helpers/expectThrow.js')
const Identity = artifacts.require('./contracts/Identity.sol')
const IdManager = artifacts.require('./contracts/IdManager.sol')

const OPERATION_CALL = 0

contract('idManager', (accounts) => {
  let idManager

  // Certificate Authorities
  const owner = accounts[0]
  const ca1 = accounts[1]
  const ca2 = accounts[2]
  const id1 = accounts[3]

  describe('Deploy contract and add identities', () => {
    it('Should Deploy the Factory Contract', async () => {
      idManager = await IdManager.new()
      expect(await idManager.owner()).to.be.equal(owner)
    })

    it('Should Add a new Identity', async () => {
      // Add identity
      const { logs } = await idManager.createIdentity('ca1', ca1)
      expect(Number(await idManager.numIdentifiers())).to.be.equal(1)
      let identity = await idManager.resolve('ca1')
      expectEvent.inLogs(logs, 'newIdentity', { did: identity[0], handler: 'ca1' })
      expect(Number(identity[1])).to.be.equal(1)

      // Check Identity contract.
      const identifier = await Identity.at(identity[0])
      expect(await identifier.owner()).to.be.equal(ca1)
    })

    it('Adding another Identity should increase the number of identities', async () => {
      await idManager.createIdentity('ca2', ca2)
      expect(Number(await idManager.numIdentifiers())).to.be.equal(2)
    })

    it('Should Not duplicate handlers', async () => {
      await expectThrow(idManager.createIdentity('ca2', ca2))
    })
  })

  describe('Work with authorities', () => {
    it('Should add one authority - with level 3', async () => {
      // add autority 1. Max level 3.
      const { logs } = await idManager.setAuthority('ca1', 3)
      expectEvent.inLogs(logs, 'changeAuthority', { handler: 'ca1', level: new BN(3) })
    })
    it('Should Not be able to set an aurhotity of not the owner', async () => {
      // Only owner can add authorities.
      await expectThrow(idManager.setAuthority('ca2', 9, { from: ca2 }))
    })

    it('Should add one authority - with level 9', async () => {
      // add autority 2. Max level 9.
      const { logs } = await idManager.setAuthority('ca2', 9)
      expectEvent.inLogs(logs, 'changeAuthority', { handler: 'ca2', level: new BN(9) })
    })

    it('Should set the level for a user', async () => {
      // Set Level to a user.
      await idManager.createIdentity('id1', id1)
      let identity = await idManager.resolve('ca1')
      const authority1 = await Identity.at(identity[0])

      const iface = new ethers.utils.Interface(idManager.abi)
      const call1 = iface.functions.setLevel.encode(['id1', '3'])
      await authority1.execute(OPERATION_CALL, idManager.address, 0, call1, { from: ca1 })
      identity = await idManager.resolve('id1')
      expect(Number(identity[1])).to.be.equal(3)

      // Try to set a level higher than the actual permissions. And fail.
      const call2 = iface.functions.setLevel.encode(['id1', '6'])
      authority1.execute(OPERATION_CALL, idManager.address, 0, call2, { from: ca1 })
      identity = await idManager.resolve('id1')
      expect(Number(identity[1])).to.be.equal(3)
    })

    it('Should remove authorities', async () => {
      await idManager.setAuthority('ca1', 0)
      await expectThrow(idManager.setLevel('id1', 6, { from: ca1 }))
    })
  })
})
