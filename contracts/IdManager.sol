pragma solidity ^0.5.4;
import "./Identity.sol";
import "./Ownable.sol";

/// @author Caelum Labs
/// @title Identity Factory
contract IdManager is Ownable{

    // Events.
    event newIdentity(address indexed did, string handler);
    event changeAuthority(string handler, uint8 level);
    event changeLevel(string handler, address indexed authority, uint8 level);

    /// Number of identities ever created.
    uint32 public numIdentifiers = 0;

    /// Identity Object.
    struct Identifier {
        address did;    // person delegated to
        uint version;   // index of the voted proposal
        uint8 level;     // Verification level
    }
    mapping(bytes32=>Identifier) identifiers;

    /// Whitellist of Certificate Authorities.
    mapping(address=>uint8) authorities;

    /// Hashes a handler
    /// @param _handler The NS handler to query.
    /// @return The associated level.
    function hashHandler(string memory _handler) internal pure returns (bytes32) {
        return keccak256(abi.encodePacked(_handler));
    }

    /// Create Identity
    /// @param _handler The NS handler to query.
    /// @param _owner Address owning the identity.
    /// @return The associated level.
    /// @dev deploys a new Identity contract and pushes it to the list of identities.
    function createIdentity(string memory _handler, address _owner) public {
        bytes32 handler = hashHandler(_handler);
        assert(identifiers[handler].version == 0);
        address newId = address(new Identity(_owner));
        identifiers[handler] = Identifier({
            did: newId,
            version: 1,
            level: 1
        });
        numIdentifiers++;
        emit newIdentity(newId, _handler);
    }

    /// Returns the address associated with an ENS node.
    /// @param _handler The NS handler to query.
    /// @return The associated address.
    function resolve(string memory _handler) public view returns (address, uint8) {
        bytes32 handler = hashHandler(_handler);
        return (identifiers[handler].did, identifiers[handler].level);
    }

    /// Set max level for an authority.
    /// @param _handler The authority handler.
    /// @param maxLevel max Level to be given by this authority.
    /// @dev Manages CAs levels (KYC)
    function setAuthority(string memory _handler, uint8 maxLevel) public onlyOwner {
        bytes32 handler = hashHandler(_handler);
        address did = identifiers[handler].did;
        authorities[did] = maxLevel;
        emit changeAuthority(_handler, maxLevel);
    }

    /// Set authority level for a handler.
    /// @param _handler The NS handler to query.
    /// @param level Level to be assigned to a handler.
    /// @dev Manages users levels : 1-unknown, 2-email&phone, 3-social media.
    function setLevel(string memory _handler, uint8 level) public {
        bytes32 handler = hashHandler(_handler);
        require(authorities[msg.sender] >= level, "CA: caller is not an authorized CA");
        identifiers[handler].level = level;
        emit changeLevel(_handler, msg.sender, level);
    }
}